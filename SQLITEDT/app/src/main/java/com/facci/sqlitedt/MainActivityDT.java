package com.facci.sqlitedt;

import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivityDT extends AppCompatActivity {

    DBHelper dbSQLITE;

    EditText txtNombre,txtApellido,txtSemestre,txtID;

    Button btnInsertar,btnModificar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dt);

        dbSQLITE = new DBHelper(this);

    }

    public void insertarClick(View v){

        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtApellido = (EditText) findViewById(R.id.txtApellido);
        txtSemestre = (EditText) findViewById(R.id.txtSemestre);

       boolean estaInsertado = dbSQLITE.insertar(txtNombre.getText().toString(),txtApellido.getText().toString(),Integer.parseInt(txtSemestre.getText().toString()));

        if(estaInsertado)
            Toast.makeText(MainActivityDT.this,"Datos Ingresados",Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(MainActivityDT.this,"Lo sentimos ocurrió un error",Toast.LENGTH_SHORT).show();

    }

    public void verTodosClick(View v){

        Cursor res = dbSQLITE.selectVerTodos();
        if(res.getCount() == 0){
            mostrarMensaje("Error","No se encontraron registros");
            return;
        }

        StringBuffer buffer = new StringBuffer();

        while(res.moveToNext()){
            buffer.append("Id : "+res.getString(0)+"\n");
            buffer.append("Nombre : "+res.getString(1)+"\n");
            buffer.append("Apellido : "+res.getString(2)+"\n");
            buffer.append("Semestre : "+res.getInt(3)+"\n\n");
        }

        mostrarMensaje("Registros",buffer.toString());
    }

    public void mostrarMensaje(String titulo, String Mensaje){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(titulo);
        builder.setMessage(Mensaje);
        builder.show();

    }

    public void modificarRegistroClick(View v){

        //Obtener los valores desde los controles
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtApellido = (EditText) findViewById(R.id.txtApellido);
        txtSemestre = (EditText) findViewById(R.id.txtSemestre);
        txtID = (EditText) findViewById(R.id.txtID);

        //llamamos al método modificarRegistro de la clase DBHelper y si la actualización es exitosa nos devolverá un valor true
        boolean estaAcutalizado = dbSQLITE.modificarRegistro(txtID.getText().toString(),txtNombre.getText().toString(),txtApellido.getText().toString(),Integer.parseInt(txtSemestre.getText().toString()));

        //Si el valor es true mostrar un mensaje al usuario
        if (estaAcutalizado == true){
            Toast.makeText(MainActivityDT.this,"Registro Actualizado",Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(MainActivityDT.this,"ERROR: Registro NO Actualizado",Toast.LENGTH_SHORT).show();
        }
    }


    //Crear un método para obtener el evento click de Eliminar
    public void eliminarRegistroClick(View v){

        //obtenemos el valor del control txtID
        txtID = (EditText) findViewById(R.id.txtID);

        //el método eliminarRegistro de la clase DBHelper retorna un entero con el número de registros
        //eliminados
        Integer registrosEliminados = dbSQLITE.eliminarRegistro(txtID.getText().toString());

        if(registrosEliminados > 0 ){
            Toast.makeText(MainActivityDT.this,"Registro(s) Eliminado(s)",Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(MainActivityDT.this,"ERROR: Registro no eliminado",Toast.LENGTH_SHORT).show();
        }

    }

}
