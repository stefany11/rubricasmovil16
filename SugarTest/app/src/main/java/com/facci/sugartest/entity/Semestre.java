package com.facci.sugartest.entity;



import com.orm.SugarRecord;

/**
 * Created by dtoala on 8/22/16.
 */
public class Semestre extends SugarRecord{
    public String nombre;
    public String apellido;
    public Integer semestre;

    public Semestre(){

    }

    public Semestre(String nombre, String apellido, Integer semestre){
        this.nombre = nombre;
        this.apellido = apellido;
        this.semestre = semestre;
    }

}
